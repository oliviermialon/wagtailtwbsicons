"""
wagtailtwbsicons

A wagtail package to add Twitter's Bootstrap Icons to administration.
"""

__version__ = "1.0.1"
__author__ = 'Olivier Mialon'
